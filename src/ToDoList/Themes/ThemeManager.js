import { DarkTheme } from "./DarkThemes";
import { LightTheme } from "./LightThemes";
import { PimaryTheme } from "./PrimaryThemes";

export const arrTheme = [
    {
        id: 1,
        name: 'Dark theme',
        theme: DarkTheme,
    },
    {
        id: 2,
        name: 'Light theme',
        theme: LightTheme,
    },
    {
        id: 3,
        name: 'Pymary theme',
        theme: PimaryTheme,
    }
]