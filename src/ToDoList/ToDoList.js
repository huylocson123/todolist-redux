import React, { Component } from 'react';
import { Container } from './components/Container';
import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { DarkTheme } from './Themes/DarkThemes'
import { LightTheme } from './Themes/LightThemes'
import { PimaryTheme } from './Themes/PrimaryThemes'
import { Dropdown } from './components/Dropdown'
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from './components/Heading'
import { TextField, abel, Input } from './components/TextField'
import { Button } from './components/Button'
import { Table, Tr, Td, Th, Thead, Tbody } from './components/Table'
import { connect } from 'react-redux';
import { addTaskAction, changeThemeAction, deleteTaskAction, doneTaskAction } from './redux/action/ToDoListActions';
import { arrTheme } from './Themes/ThemeManager';
class ToDoList extends Component {
    state = {
        taskName: '',
    }

    renderTaskToDo = () => {
        return this.props.taskList.filter(task => !task.done).map((task, index) => {
            return <Tr key={index}>
                <Th styled={{ verticalAlign: `middle` }}>{task.taskName}</Th>
                <Th className="text-end">
                    <Button><i className="fa fa-edit"></i></Button>
                    <Button onClick={() => {
                        this.props.dispatch(doneTaskAction(task.id))
                    }}><i className="fa fa-check"></i></Button>
                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id))
                    }}><i className="fa fa-trash"></i></Button>
                </Th>
            </Tr>
        })
    }
    renderTaskCompleted = () => {
        return this.props.taskList.filter(task => task.done).map((task, index) => {
            return <Tr key={index}>
                <Th styled={{ verticalAlign: `middle` }}>{task.taskName}</Th>
                <Th className="text-end">
                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id))
                    }}><i className="fa fa-trash"></i></Button>
                </Th>
            </Tr>
        })
    }
    renderTheme = () => {
        return arrTheme.map((theme, index) => {
            return <option value={theme.id}>{theme.name}</option>
        })
    }
    render() {
        return (
            <ThemeProvider theme={this.props.themeToDoList}>
                <Container className="w-50">
                    <Dropdown onChange={(e) => {
                        let { value } = e.target;
                        this.props.dispatch(changeThemeAction(value))
                    }}>
                        {this.renderTheme()}
                    </Dropdown>
                    <Heading2>To Do List</Heading2>
                    <TextField onChange={(e) => {
                        this.setState({
                            taskName: e.target.value,

                        })

                    }} label="Task name "></TextField>
                    <Button
                        onClick={() => {
                            //lay thong tin tư2 in put
                            let { taskName } = this.state;
                            // tao ra 1 task onject
                            let newTask = {
                                id: Date.now(),
                                taskName: taskName,
                                done: false
                            }

                            this.props.dispatch(addTaskAction(newTask))
                            //dua task op lên redux
                        }
                        }
                        className="ms-2"><i className="fa fa-plus"></i> Add ask</Button>
                    <Button className="ms-2"><i className="fa fa-upload"></i> UpDate task</Button>
                    <hr />
                    <Heading3>Task To Do</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskToDo()}

                        </Thead>
                    </Table>
                    <Heading3>Task completed</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskCompleted()}

                        </Thead>
                    </Table>
                </Container>
            </ThemeProvider>
        );
    }
}


const mapStateToProps = state => {
    return {
        themeToDoList: state.ToDoListReducer.themeToDoList,
        taskList: state.ToDoListReducer.taskList,
    }
}

export default connect(mapStateToProps)(ToDoList)