import { add_task, change_theme, delete_task, done_task } from "../type/ToDoListType";


export const addTaskAction = (newTask) => ({
    type: add_task,
    newTask,
})

export const changeThemeAction = (themeId) => ({
    type: change_theme,
    themeId,
})

export const doneTaskAction = (taskId) => ({
    type: done_task,
    taskId,
})
//({return}) = ({object});
export const deleteTaskAction = (taskId) => ({
    type: delete_task,
    taskId,
})