import { DarkTheme } from "../../Themes/DarkThemes"
import { add_task, change_theme, delete_task, done_task } from "../type/ToDoListType"
import { arrTheme } from "../../Themes/ThemeManager"
const initialState = {
    themeToDoList: DarkTheme,
    taskList: [],
}

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initialState, action) => {
    switch (action.type) {
        case add_task: {
            if (action.newTask.taskName.trim() === '') {
                alert('Task name is required!');
                return { ...state }
            }
            let taskListUpdate = [...state.taskList];
            let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
            if (index !== -1) {
                alert('Task name already exist!');
                return { ...state }
            }
            taskListUpdate.push(action.newTask)
            state.taskList = taskListUpdate;
            return { ...state }
        }
        case change_theme: {
            let theme = arrTheme.find(theme => theme.id == action.themeId);
            if (theme) {
                state.themeToDoList = { ...theme.theme };
            }

            return { ...state }
        }
        case done_task: {
            let taskListUpdate = [...state.taskList]
            let index = taskListUpdate.findIndex(task => task.id === action.taskId)
            if (index !== -1) {
                taskListUpdate[index].done = true;
            }
            // state.taskList = taskListUpdate
            return { ...state, taskList: taskListUpdate }
        }
        case delete_task: {
            // let taskListUpdate = [...state.taskList]
            // taskListUpdate = taskListUpdate.filter(task => task.id !== action.id);
            // return { ...state, taskList: taskListUpdate }
            return { ...state, taskList: state.taskList.filter(task => task.id !== action.taskId) }
        }

        // eslint-disable-next-line no-fallthrough
        default:
            return { ...state }
    }
}
